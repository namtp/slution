import cv2
import numpy as np


 
def load_pts(path):
    pp = []
    with open(path) as f:
        rows = [rows.strip() for rows in f]

    """Use the curly braces to find the start and end of the point data"""
    head = rows.index('{') + 1
    tail = rows.index('}')

    """Select the point data split into coordinates"""
    raw_points = rows[head:tail]
    coords_set = [point.split() for point in raw_points]

    """Convert entries from lists of strings to tuples of floats"""
    points = [tuple([float(point) for point in coords]) for coords in coords_set]

    return points


def  draw_line (point1 ,point2 , image):
        image = cv2.line(image,(int(point1[0]),int(point1[1])),(int(point2[0]),int(point2[1])),(255,255,255),5)
        return image

def draw_lines(list_point , image):
    for  i in range(0,len(list_point)-1):
        image  = draw_line(list_point[i],list_point[i+1] ,image)
    return image

   
def draw_point(points , image):
    for p in points:
        cv2.circle(image, (int(p[0]), int(p[1])), 5, (135, 206, 235), -1) 
    # draw_line((121,748) ,(249,772),image)
    return image

def fx (input  ,  point1  ,  point2):
    fx = (input[0]-point1[0])/(point2[0]-point1[0]) - (input[1]-point1[1])/(point2[1]-point1[1])
    if fx <= 0 :
        return 0 
    else:
        return 1

def  load_image(path):
    return cv2.imread(path)

def divide (decesion_point , points):
    left = []
    right = []
    for point in  points:
        if fx(list(point) ,decesion_point[0], decesion_point[1]) <= 0 & fx(list(point) ,decesion_point[1], decesion_point[2]) <=0 :
            left.append(point)
        else:
            right.append(point)
    return left , right


        

if __name__ == '__main__':
    PATH_POINTS = '/home/namtp/Desktop/Nice_date/points.pts'
    PATH_DECISION_POINT  =  '/home/namtp/Desktop/Nice_date/decision_points.pts'
    image = load_image('/home/namtp/Desktop/Nice_date/image.jpg')
    decesion_point =  load_pts(PATH_DECISION_POINT)
    points  =  load_pts(PATH_POINTS)
    left  , right = divide (decesion_point , points)
    image = draw_point(right , image)
    image = draw_lines(decesion_point , image)
    cv2.imshow('image', image)
    cv2.waitKey(0)


