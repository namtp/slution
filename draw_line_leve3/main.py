 
import cv2
import numpy as np
import  matplotlib.pyplot as  plt 


def load_pts(path):
    pp = []
    with open(path) as f:
        rows = [rows.strip() for rows in f]

    """Use the curly braces to find the start and end of the point data"""
    head = rows.index('{') + 1
    tail = rows.index('}')

    """Select the point data split into coordinates"""
    raw_points = rows[head:tail]
    coords_set = [point.split() for point in raw_points]

    """Convert entries from lists of strings to tuples of floats"""
    points = [tuple([float(point) for point in coords]) for coords in coords_set]
    x  = []
    y  =  []
    for  item  in points:
        x.append(item[0])
        y.append(item[1])
    return x , y


def polyfit(X  , Y ,lever):
    z = np.polyfit(X, Y,lever)
    p = np.poly1d(z)
    xp = np.linspace(100, 3000, 400)
    plt.plot(x, y, '.', xp, p(xp), '-')
    plt.ylim(100,3000)
    plt.show()

if __name__ == '__main__':
    PATH_POINT  =  '/home/namtp/Desktop/work/draw line  level 3/points3n.pts'
    x  , y  = load_pts(PATH_POINT)
    polyfit(x,y,3)
